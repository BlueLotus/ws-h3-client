package ntust.idsl.book.client.main;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import ntust.idsl.book.client.view.ClientView;

/**
 * @author Carl Adler(C.A.)
 * */
public class Main {
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
            public void run() {
				UIManager.put("swing.boldMetal", Boolean.FALSE);
				createAndShowGUI();
            }
        });
	}
	
	private static void createAndShowGUI() {
        JFrame frame = new JFrame("TabbedPaneDemo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		frame.add(new ClientView(), BorderLayout.CENTER);
        frame.pack();
        frame.setVisible(true);
    }
}
