package ntust.idsl.book.client.cryp.dsa;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.security.PublicKey;
import java.security.Signature;

import org.apache.commons.codec.binary.Base64;

import ntust.idsl.book.client.cryp.util.FileNameGenerator;

/**
 * @author Carl Adler (C.A.)
 * */
public class DSASignatureVerifier {
	
	private static ObjectInputStream objInputStream;
	private static PublicKey publicKey;
	private static boolean result = false;
	
	public static boolean verifySignature(Signature signature, String signedMessage, String encryptedMessage) {
		System.out.println("	--- Verify Signature Process Started ---");
		try {
			initializePublicKey();
			verifySignatureWithMessage(signature, signedMessage, encryptedMessage);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("	--- Verify Signature Process Finished ---\n\n");
		return result;
	}
	
	private static void initializePublicKey() throws Exception {
		objInputStream = new ObjectInputStream(new FileInputStream(FileNameGenerator.PUBLIC_KEY_FOR_SERVER));
		publicKey = (PublicKey) objInputStream.readObject();
		objInputStream.close();
	}
	
	private static void verifySignatureWithMessage(Signature signature, String signedMessage, String encryptedMessage) throws Exception {
		signature.initVerify(publicKey);
		signature.update(encryptedMessage.getBytes());
		byte[] raw = Base64.decodeBase64(signedMessage);
		System.out.println("		Encrypted Message: " + encryptedMessage);
		System.out.println("		Signed message needs to verified: " + signedMessage);
		if(signature.verify(raw)) {
			System.out.println("		Verify signature successfully.");
			result = true;
		} else {
			System.out.println("		Verify signature failed.");
			throw new Exception("Verify Failed.");
		}
	}
}
