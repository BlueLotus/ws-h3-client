package ntust.idsl.book.client.cryp.dsa;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.security.PrivateKey;
import java.security.Signature;

import org.apache.commons.codec.binary.Base64;

import ntust.idsl.book.client.cryp.util.FileNameGenerator;


/**
 * @author Carl Adler (C.A.)
 * */
public class DSASignatureSigner {
	
	private static ObjectInputStream objInputStream = null;
	private static PrivateKey privateKey;
	
	public static String signSignature(Signature signature, String encryptedMessage) {
		String signedMessage = null;
		System.out.println("--- Sign Message Process Started ---");
		try {
			initializePrivateKey();
			signedMessage = signMessage(signature, encryptedMessage);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("--- Sign Message Process Finished ---\n");
		return signedMessage;
	}
	
	private static void initializePrivateKey() throws Exception {
		objInputStream = new ObjectInputStream(new FileInputStream(FileNameGenerator.PRIVATE_KEY_FOR_CLIENT));
		privateKey = (PrivateKey)objInputStream.readObject();
		objInputStream.close();
	}
	
	private static String signMessage(Signature signature, String encryptedMessage) throws Exception {
		System.out.println("Encrypted message: " + encryptedMessage);
		signature.initSign(privateKey);
		signature.update(encryptedMessage.getBytes());
		byte[] raw = signature.sign();
		String result = Base64.encodeBase64String(raw);
		System.out.println("Message after signed: " + result);
		return result;
	}
}
