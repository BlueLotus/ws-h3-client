package ntust.idsl.book.client.cryp.aes;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * @author Carl Adler (C.A.)
 * */
public class AESDecryptor {
	
	private static Cipher cipher;
	private static SecretKeySpec secretKey;

	public static String decryptForStringWithCBCMode(String strToDecrypt, byte[] key,  String initialVector) {
		try {
			cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			secretKey = new SecretKeySpec(key, "AES");
			IvParameterSpec iv = new IvParameterSpec(initialVector.getBytes());
			cipher.init(Cipher.DECRYPT_MODE, secretKey, iv);
			return new String(cipher.doFinal(Base64.decodeBase64(strToDecrypt)));
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}

}
