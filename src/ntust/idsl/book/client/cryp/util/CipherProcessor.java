package ntust.idsl.book.client.cryp.util;

import java.security.NoSuchAlgorithmException;
import java.security.Signature;

import ntust.idsl.book.client.cryp.aes.AESDecryptor;
import ntust.idsl.book.client.cryp.aes.AESEncryptor;
import ntust.idsl.book.client.cryp.aes.AESKeyGenerator;
import ntust.idsl.book.client.cryp.dsa.DSASignatureSigner;
import ntust.idsl.book.client.cryp.dsa.DSASignatureVerifier;
import ntust.idsl.book.client.stub.BookServiceImplStub.PurchaseHistory;

/**
 * @author Carl Adler(C.A.)
 * */
public class CipherProcessor {
	
	private byte[] secretKey;
	private String initialVector;
	private Signature signature;
	
	public CipherProcessor() {
		try {
			System.out.println("Cipher material initializing...\n");
			secretKey = AESKeyGenerator.obtainKeyFromFile();
			initialVector = "3102843462069914";
			signature = Signature.getInstance("DSA");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}
	
	public String encryptMessage(String message) {
		System.out.println("	Message to be encrypted: " + message);
		System.out.println("	Secret Key: " + new String(secretKey));
		String encryptedMessage = AESEncryptor.encryptForStringWithCBCMode(message, secretKey, initialVector);
		System.out.println("	Message after encrypted: " + encryptedMessage + "\n");
		return encryptedMessage;
	}
	
	public String signTheEncryptedMessage(String encryptedMessage) {
		return DSASignatureSigner.signSignature(signature, encryptedMessage);
	}
	
	public String verifySignedMessageAndDecrypt(String signedMessage,String encryptedMessage, String fieldName) throws Exception {
		String decryptedString;
		if(DSASignatureVerifier.verifySignature(signature, signedMessage, encryptedMessage)) {			
			decryptedString = AESDecryptor.decryptForStringWithCBCMode(encryptedMessage, AESKeyGenerator.obtainKeyFromFile(), initialVector);
			System.out.printf("	%s after decrypted: " + decryptedString + "\n\n", fieldName);
		} else {
			throw new Exception("Verify Failed.");
		}
		return decryptedString;
	}
	
	public void verifyPurchaseHistory(PurchaseHistory purchaseHistory) {
		try {
			verifySignedMessageAndDecrypt(purchaseHistory.getSignedMemberID(), purchaseHistory.getEncryptedMemberID(), "MemberID");
			verifySignedMessageAndDecrypt(purchaseHistory.getSignedBookID(), purchaseHistory.getEncryptedBookID(), "BookID");
			verifySignedMessageAndDecrypt(purchaseHistory.getSignedAmount(), purchaseHistory.getEncryptedAmount(), "Amount");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}