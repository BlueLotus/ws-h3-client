package ntust.idsl.book.client.view;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.table.TableColumnModel;

import ntust.idsl.book.client.cryp.util.CipherProcessor;
import ntust.idsl.book.client.model.PurchaseHistoryModel;
import ntust.idsl.book.client.module.PurchaseHistoryModule;
import ntust.idsl.book.client.stub.BookServiceImplStub.PurchaseHistory;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * @author Carl Adler(C.A.)
 * */
public class PurchaseOrderPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private static int memberId;
	private static String memberName = "";
	private JLabel memberNameLabel = new JLabel();
	private JButton showHistoryButton;
	private JTable purchaseOrderTable = null;
	private JScrollPane scrollPane = null;
	private PurchaseHistoryModel purchaseHistoryModel = null;
	private CipherProcessor cipherProcessor = new CipherProcessor();
	
	public PurchaseOrderPanel() {
		setLayout(null);
		initializeLabel();
		initializeButton();
	}
	
	private void initializeLabel() {
		memberNameLabel = new JLabel("Member Name: " + memberName);
		memberNameLabel.setFont(new Font("微軟正黑體", Font.PLAIN, 15));
		memberNameLabel.setBounds(10, 10, 191, 26);
		add(memberNameLabel);
	}
	
	private void updateMemberNameLabel() {
		memberNameLabel.setText("Member Name: " + memberName);
	}
	
	private void initializeButton() {
		showHistoryButton = new JButton("Show/Update History");
        showHistoryButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		updateMemberNameLabel();
        		initializeTableWithMemberId(memberId);
        	}
        });
        showHistoryButton.setFont(new Font("微軟正黑體", Font.PLAIN, 15));
        showHistoryButton.setBounds(411, 15, 191, 23);
        add(showHistoryButton);
	}
	
	private void initializeTableWithMemberId(int memberId) {
		purchaseHistoryModel = new PurchaseHistoryModel();
		purchaseOrderTable = new JTable(purchaseHistoryModel);
		TableColumnModel tcm = purchaseOrderTable.getColumnModel();
		tcm.getColumn(0).setPreferredWidth(25);
        tcm.getColumn(1).setPreferredWidth(25);
        tcm.getColumn(2).setPreferredWidth(25);
        tcm.getColumn(3).setPreferredWidth(25);
        tcm.getColumn(4).setPreferredWidth(25);
        tcm.getColumn(5).setPreferredWidth(25);
        tcm.getColumn(6).setPreferredWidth(25);
        tcm.getColumn(7).setPreferredWidth(25);
        tcm.getColumn(8).setPreferredWidth(25);
        tcm.getColumn(9).setPreferredWidth(25);
        scrollPane = new JScrollPane(purchaseOrderTable);
        scrollPane.setBounds(21, 62, 581, 350);
        try {
			addDataWithMemberId(memberId);
		} catch (Exception e) {
			e.printStackTrace();
		}
        purchaseOrderTable.setBorder(new LineBorder(new Color(0, 0, 0), 0));
        purchaseOrderTable.setFont(new Font("微軟正黑體", Font.PLAIN, 12));
        purchaseOrderTable.setBounds(34, 276, 372, 186);
        purchaseOrderTable.setBackground(Color.white);
        add(scrollPane);
	}
	
	private void addDataWithMemberId(int memberId) throws Exception {
		PurchaseHistory[] purchaseHistories = PurchaseHistoryModule.getPurchaseHistoryForMember(memberId);
		for (PurchaseHistory purchaseHistory : purchaseHistories) {
			purchaseHistoryModel.addRow(purchaseHistory);
			cipherProcessor.verifyPurchaseHistory(purchaseHistory);
		}
		purchaseOrderTable.updateUI();
	}

	public static void setMemberId(int inputMemberId) {
		memberId = inputMemberId;
	}
	
	public static void setMemberName(String inputMemberName) {
		memberName = inputMemberName;
	}
}