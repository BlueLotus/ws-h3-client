package ntust.idsl.book.client.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import ntust.idsl.book.client.module.LoginModule;

/**
 * @author Carl Adler(C.A.)
 * */
public class LoginPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private JLabel userNameLabel;
	private JLabel passwordLabel;
	private JLabel titleLabel;
	private JTextField userNameField;
	private JPasswordField passwordField;
	private JButton loginButton;
	private JButton cancelButton;
	private boolean succeeded;
	
	public LoginPanel() {
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setLayout(null);
		initializeLabel();
		initializeField();
		initializeLoginButton();
		initializeCancelButton();
		setPreferredSize(new Dimension(500, 600));
	}
	
	private void initializeLabel() {
		titleLabel = new JLabel("IDSL Book Store Client");
		titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
		titleLabel.setFont(new Font("微軟正黑體", Font.PLAIN, 15));
		titleLabel.setBounds(107, 70, 271, 15);
		add(titleLabel);
		
		userNameLabel = new JLabel("User Name");
		userNameLabel.setFont(new Font("微軟正黑體", Font.PLAIN, 12));
		userNameLabel.setBounds(119, 135, 82, 15);
		add(userNameLabel);
		
		passwordLabel = new JLabel("Password");
		passwordLabel.setFont(new Font("微軟正黑體", Font.PLAIN, 12));
		passwordLabel.setBounds(119, 188, 82, 15);
		add(passwordLabel);
	}
	
	private void initializeField() {
		userNameField = new JTextField();
		userNameField.setBounds(233, 133, 145, 21);
		add(userNameField);
		userNameField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(233, 186, 145, 21);
		add(passwordField);
	}
	
	private void initializeLoginButton() {
		loginButton = new JButton("Login");
		loginButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if(LoginModule.authenticate(getUserName(), getPassword())) {
						JOptionPane.showMessageDialog(new JDialog(), 
								"Hi, " + getUserName() + "! You have successfully logged in.",
								"Login",
								JOptionPane.INFORMATION_MESSAGE);
						succeeded = true;
						OrderPanel.setMemberId(LoginModule.getMemberId());
						PurchaseOrderPanel.setMemberId(LoginModule.getMemberId());
						PurchaseOrderPanel.setMemberName(getUserName());
					}
					else {
						JOptionPane.showMessageDialog(new JDialog(), 
								"Invalid usename or password", "Login", 
								JOptionPane.ERROR_MESSAGE);
						cleanTextFieldContent();
						succeeded = false;
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		loginButton.setFont(new Font("微軟正黑體", Font.PLAIN, 12));
		loginButton.setBounds(119, 252, 87, 23);
		add(loginButton);
	}
	
	private void initializeCancelButton() {
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cleanTextFieldContent();
			}
		});
		cancelButton.setFont(new Font("微軟正黑體", Font.PLAIN, 12));
		cancelButton.setBounds(291, 252, 87, 23);
		add(cancelButton);
	}	
	
	public String getUserName() {
		return userNameField.getText().trim();
	}

	public String getPassword() {
		return new String(passwordField.getPassword());
	}
	
	public boolean isSucceeded() {
		return succeeded;
	}
	
	private void cleanTextFieldContent() {
		userNameField.setText("");
		passwordField.setText("");
	}
	
}
