package ntust.idsl.book.client.view;

import java.awt.GridLayout;
import java.awt.event.KeyEvent;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 * @author Carl Adler(C.A.)
 * */
public class ClientView extends JPanel {

	private static final long serialVersionUID = 1L;

	public ClientView() {
		super(new GridLayout(1, 1));
        
        JTabbedPane tabbedPane = new JTabbedPane();
        
        LoginPanel loginPanel = new LoginPanel();
        tabbedPane.addTab("Login", loginPanel);
        tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);
        
        OrderPanel orderPanel = new OrderPanel();
        tabbedPane.addTab("Order", orderPanel);
        tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);
        
        PurchaseOrderPanel purchaseOrderPanel = new PurchaseOrderPanel();
        tabbedPane.addTab("Purchase History", purchaseOrderPanel);
        tabbedPane.setMnemonicAt(2, KeyEvent.VK_3);
        
        add(tabbedPane);
        
        tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
	}
	
	protected JComponent makeTextPanel(String text) {
        JPanel panel = new JPanel(false);
        JLabel filler = new JLabel(text);
        filler.setHorizontalAlignment(JLabel.CENTER);
        panel.setLayout(new GridLayout(1, 1));
        panel.add(filler);
        return panel;
    }

}
