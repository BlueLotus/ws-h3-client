package ntust.idsl.book.client.view;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.table.TableColumnModel;

import ntust.idsl.book.client.cryp.util.CipherProcessor;
import ntust.idsl.book.client.model.BookModel;
import ntust.idsl.book.client.module.BookListModule;
import ntust.idsl.book.client.module.DeliveryMethodListModule;
import ntust.idsl.book.client.module.OrderModule;
import ntust.idsl.book.client.stub.BookServiceImplStub.BookInfo;
import ntust.idsl.book.client.stub.BookServiceImplStub.EncryptBookOrder;
import ntust.idsl.book.client.stub.BookServiceImplStub.SignedOrderInfo;

import java.awt.Color;

import javax.swing.JButton;

/**
 * @author Carl Adler(C.A.)
 * */
public class OrderPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private static int memberId;
	private int deliveryIndex;
	private static Date date = new Date();
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	private static String[] deliveryMethodSet = DeliveryMethodListModule.obtainDeliveryMethods();
	private JButton orderButton;
	private JButton cancelButton;
	private JLabel dateLabel;
	private JLabel deliveryMethodLabel;
	
	@SuppressWarnings("rawtypes")
	private JComboBox deliveryComboBox;
	private JTable bookTable;
	private JScrollPane scrollPane;
	private BookModel bookModel;
	private CipherProcessor cipherProcessor = new CipherProcessor();
	private EncryptBookOrder[] encryptBookOrders;
	private SignedOrderInfo[] signedOrderInfos;
	
	public OrderPanel() {
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(null);
		initializeLabel();
		initializeOrderButton();
		initializeCancelButton();
		initializeDeliveryBox();
		initializeTable();
	}
	
	private void initializeOrderButton() {
		orderButton = new JButton("Order");
		orderButton.setFont(new Font("微軟正黑體", Font.PLAIN, 12));
		orderButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					System.out.println(sendOrder());
				} catch (Exception sendException) {
					System.out.println("Send order operation fail.");
					sendException.printStackTrace();
				}
			}
		});
		orderButton.setBounds(116, 474, 87, 23);
		add(orderButton);
	}
	
	private void initializeCancelButton() {
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				initializeTable();
			}
		});
		cancelButton.setFont(new Font("微軟正黑體", Font.PLAIN, 12));
		cancelButton.setBounds(271, 474, 87, 23);
		add(cancelButton);
	}
	
	private void initializeLabel() {
		dateLabel = new JLabel("Date: " + dateFormat.format(new Date(date.getTime())));
		dateLabel.setBounds(48, 10, 123, 21);
		dateLabel.setFont(new Font("微軟正黑體", Font.PLAIN, 15));
		add(dateLabel);
		
		deliveryMethodLabel = new JLabel("Delivery Method");
		deliveryMethodLabel.setBounds(242, 10, 116, 21);
		deliveryMethodLabel.setFont(new Font("微軟正黑體", Font.PLAIN, 15));
		add(deliveryMethodLabel);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initializeDeliveryBox() {
		deliveryComboBox = new JComboBox(deliveryMethodSet);
		deliveryComboBox.setBounds(379, 10, 94, 21);
		deliveryComboBox.insertItemAt("Choose", 0);
		deliveryComboBox.setSelectedIndex(0);
		deliveryComboBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				String deliverMethod = (String) e.getItem();
				if(deliverMethod.equals("DHL")){
					deliveryIndex = 1;
				} else if(deliverMethod.equals("FedEx")) {
					deliveryIndex = 2;
				} else {
					deliveryIndex = 0;
				}
				System.out.println("DeliveryIndex = " + deliveryIndex);
			}
		});
		add(deliveryComboBox);
	}
	
	private void initializeTable() {
		bookModel  = new BookModel();
		bookTable = new JTable(bookModel);
		TableColumnModel tcm = bookTable.getColumnModel();
        tcm.getColumn(0).setPreferredWidth(50);
        tcm.getColumn(1).setPreferredWidth(50);
        tcm.getColumn(2).setPreferredWidth(50);
        tcm.getColumn(3).setPreferredWidth(15);
        tcm.getColumn(4).setPreferredWidth(8);
        tcm.getColumn(5).setPreferredWidth(8);
        scrollPane = new JScrollPane(bookTable);
        scrollPane.setBounds(21, 36, 452, 428);
        try {
			addData();
		} catch (Exception e) {
			e.printStackTrace();
		}
		bookTable.setBorder(new LineBorder(new Color(0, 0, 0), 0));
		bookTable.setFont(new Font("微軟正黑體", Font.PLAIN, 12));
		bookTable.setBounds(34, 276, 372, -186);
		bookTable.setBackground(Color.white);
		add(scrollPane);
	}

	public static void setMemberId(int inputMemberId) {
		memberId = inputMemberId;
		System.out.println("Obtain memberID: " + memberId);
	}
	
	private void addData() throws Exception {
    	BookInfo[] bookList= BookListModule.obatinBookList();
    	for (BookInfo book : bookList) {
    		bookModel.addRow(book);
		}
    	bookTable.updateUI();
    }
	
	private String sendOrder() throws Exception{
		prepareOrder();
		return OrderModule.sendOrder(encryptBookOrders, signedOrderInfos);
	}
	
	private void prepareOrder() {
		int orderAmount = countOrderAmount();
		int orderId = genOrderId();
		int orderIndex = 0;
		encryptBookOrders = new EncryptBookOrder[orderAmount];
		signedOrderInfos = new SignedOrderInfo[orderAmount];
		Long currentTime = new Date().getTime();
		for(int i = 0; i < bookModel.getRowCount(); i++){
			int currentQuantity = (int)bookModel.getValueAt(i, 5);
			if(currentQuantity > 0)
			{
				EncryptBookOrder order = new EncryptBookOrder();
				SignedOrderInfo signedOrderInfo = new SignedOrderInfo();
				int bookId = i + 1;
				String encryptMemberID = cipherProcessor.encryptMessage(String.valueOf(memberId));
				String encryptBookID = cipherProcessor.encryptMessage(String.valueOf(bookId));
				String encryptAmount = cipherProcessor.encryptMessage(String.valueOf(currentQuantity));
				order.setOrderID(orderId + orderIndex);
				order.setOrderDate(new Date(currentTime));
				order.setDeliveryID(deliveryIndex);
				order.setMemberID(encryptMemberID);
				order.setBookID(encryptBookID);
				order.setAmount(encryptAmount);
				encryptBookOrders[orderIndex] = order;
				
				signedOrderInfo.setMemberID(cipherProcessor.signTheEncryptedMessage(encryptMemberID));
				signedOrderInfo.setBookID(cipherProcessor.signTheEncryptedMessage(encryptBookID));
				signedOrderInfo.setAmount(cipherProcessor.signTheEncryptedMessage(encryptAmount));
				signedOrderInfos[orderIndex] = signedOrderInfo;
				orderIndex++;
			}
		}
	}
	
	private int countOrderAmount() {
		int orderAmount = 0;
		for(int i = 0; i < bookModel.getRowCount(); i++) {
			if((int) bookModel.getValueAt(i, 5) > 0)
				orderAmount++;
		}
		return orderAmount;
	}
	
	private int genOrderId() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMddhhmm");
		Long currentTime = new Date().getTime();
		Date date = new Date(currentTime);
		return Integer.valueOf(sdf.format(date));
	}
}
