package ntust.idsl.book.client.module;

import java.rmi.RemoteException;

import ntust.idsl.book.client.stub.BookServiceImplStub;

/**
 * @author Cael Adler(C.A.)
 * */
public class LoginModule {
	
	private static int memberId = 0;
	
	public static boolean authenticate(String username, String password) throws Exception{
		if(compareLoginInfo(username, password))
			return true;
		return false;
	}
	
	private static boolean compareLoginInfo(String username, String password) throws RemoteException{
		BookServiceImplStub stubForLogin = new BookServiceImplStub();
		BookServiceImplStub.VerifyMember verify =  new BookServiceImplStub.VerifyMember();
		verify.setAccount(username);
		verify.setPassword(password);
		boolean loginResult = false;
		memberId = stubForLogin.verifyMember(verify).get_return();
		if(memberId > 0) {
			loginResult = true;
		}
		return loginResult;
	}

	public static int getMemberId() {
		return memberId;
	}

}
