package ntust.idsl.book.client.module;

import ntust.idsl.book.client.stub.BookServiceImplStub;
import ntust.idsl.book.client.stub.BookServiceImplStub.GetDeliveryMethods;

/**
 * @author Carl Adler(C.A.)
 * */
public class DeliveryMethodListModule {
	
	public static String[] obtainDeliveryMethods() {
		String[] result = null;
		BookServiceImplStub deliveryMethodStub = null;
		try {
			deliveryMethodStub = new BookServiceImplStub();
			result = deliveryMethodStub.getDeliveryMethods(new GetDeliveryMethods()).get_return();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
}
