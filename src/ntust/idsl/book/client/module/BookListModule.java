package ntust.idsl.book.client.module;

import ntust.idsl.book.client.stub.BookServiceImplStub;
import ntust.idsl.book.client.stub.BookServiceImplStub.BookInfo;

/**
 * @author Carl Adler(C.A.)
 * */
public class BookListModule {
	
	public static BookInfo[] obatinBookList() throws Exception{
		BookServiceImplStub bookListStub = new BookServiceImplStub();
		BookInfo[] bookList = bookListStub.listAllBookInfo(new BookServiceImplStub.ListAllBookInfo()).get_return();
		return bookList;
	}
	
}
