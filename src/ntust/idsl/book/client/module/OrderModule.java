package ntust.idsl.book.client.module;

import ntust.idsl.book.client.stub.BookServiceImplStub;
import ntust.idsl.book.client.stub.BookServiceImplStub.EncryptBookOrder;
import ntust.idsl.book.client.stub.BookServiceImplStub.SaveOrder;
import ntust.idsl.book.client.stub.BookServiceImplStub.SignedOrderInfo;

/**
 * @author Carl Adler(C.A.)
 * */
public class OrderModule {
	
	public static String sendOrder(EncryptBookOrder[] encryptBookOrders, SignedOrderInfo[] signedOrderInfos) throws Exception{
		BookServiceImplStub bookOrderStub = new BookServiceImplStub();
		SaveOrder orderList = new SaveOrder();
		for(int i = 0; i < encryptBookOrders.length; i ++) {
			orderList.addEncryptBookOrders(encryptBookOrders[i]);
			orderList.addSignedOrderInfos(signedOrderInfos[i]);
		}
		return bookOrderStub.saveOrder(orderList).get_return();
	}
}
