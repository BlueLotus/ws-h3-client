package ntust.idsl.book.client.module;

import ntust.idsl.book.client.stub.BookServiceImplStub;
import ntust.idsl.book.client.stub.BookServiceImplStub.ListPurchaseHistoryForMember;
import ntust.idsl.book.client.stub.BookServiceImplStub.PurchaseHistory;

/**
 * @author Carl Adler(C.A.)
 * */
public class PurchaseHistoryModule {
	
	public static PurchaseHistory[] getPurchaseHistoryForMember(int memberId) throws Exception {
		BookServiceImplStub purchaseHistoryStub = new BookServiceImplStub();
		ListPurchaseHistoryForMember listHistoryForMember = new ListPurchaseHistoryForMember();
		listHistoryForMember.setMemberID(memberId);
		return purchaseHistoryStub.listPurchaseHistoryForMember(listHistoryForMember).get_return();
	}
	
}
