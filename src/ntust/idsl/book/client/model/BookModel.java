package ntust.idsl.book.client.model;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import ntust.idsl.book.client.stub.BookServiceImplStub.BookInfo;

/**
 * @author Carl Adler(C.A.)
 * */
public class BookModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("rawtypes")
	private Vector<Vector<Comparable>> bookContent = null;
	private String[] rowTitle = {"Title", "Author", "ISBN", "Publisher", "Price", "Quantity"};
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public BookModel() {
		bookContent = new Vector();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public BookModel(int count) {
		bookContent = new Vector(count);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void addRow(BookInfo book) {
		Vector v = new Vector(6);
		v.add(0, book.getTitle());
		v.add(1, book.getAuthor());
		v.add(2, book.getIsbn());
		v.add(3, book.getPublisher());
		v.add(4, book.getPrice());
		v.add(5, 0);
		bookContent.add(v);
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == 5) 
            return true;
        return false;
    }
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void setValueAt(Object value, int row, int col) {
        ((Vector) bookContent.get(row)).remove(col);
        ((Vector) bookContent.get(row)).add(col, value);
        this.fireTableCellUpdated(row, col);
    }
	
	@Override
	public String getColumnName(int col) {
        return rowTitle[col];
    }

	@Override
	public int getRowCount() {
		return bookContent.size();
	}

	@Override
	public int getColumnCount() {
		return rowTitle.length;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Object getValueAt(int row, int col) {
		return ((Vector) bookContent.get(row)).get(col);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Class getColumnClass(int col) {
        return getValueAt(0, col).getClass();
    }

}
