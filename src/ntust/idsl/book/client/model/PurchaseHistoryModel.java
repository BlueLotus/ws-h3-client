package ntust.idsl.book.client.model;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import ntust.idsl.book.client.stub.BookServiceImplStub.PurchaseHistory;

/**
 * @author Carl Adler(C.A.)
 * */
public class PurchaseHistoryModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("rawtypes")
	private Vector<Vector<Comparable>> purchaseHistoryContent = null;
	private String[] rowTitle = {"Order ID", "Order Date", "Title", "Author", "ISBN", "Publisher", "Price", "Quantity", "Delivery Method", "Total Price"};
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public PurchaseHistoryModel() {
		purchaseHistoryContent = new Vector();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public PurchaseHistoryModel(int count) {
		purchaseHistoryContent = new Vector(count);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void addRow(PurchaseHistory purchaseHistory) {
		Vector v = new Vector(10);
		v.add(0, purchaseHistory.getOrderID());
		v.add(1, purchaseHistory.getOrderDate());
		v.add(2, purchaseHistory.getTitle());
		v.add(3, purchaseHistory.getAuthor());
		v.add(4, purchaseHistory.getIsbn());
		v.add(5, purchaseHistory.getPublisher());
		v.add(6, purchaseHistory.getPrice());
		v.add(7, purchaseHistory.getAmount());
		v.add(8, purchaseHistory.getDeliveryName());
		v.add(9, purchaseHistory.getTotal());
		purchaseHistoryContent.add(v);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void setValueAt(Object value, int row, int col) {
		((Vector) purchaseHistoryContent.get(row)).remove(col);
		((Vector) purchaseHistoryContent.get(row)).add(col, value);
		this.fireTableCellUpdated(row, col);
	}
	
	@Override
	public String getColumnName(int col) {
        return rowTitle[col];
    }

	@Override
	public int getRowCount() {
		return purchaseHistoryContent.size();
	}

	@Override
	public int getColumnCount() {
		return rowTitle.length;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Object getValueAt(int row, int col) {
		return ((Vector) purchaseHistoryContent.get(row)).get(col);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Class getColumnClass(int col) {
		return getValueAt(0, col).getClass();
	}

}
