package ntust.idsl.book.client.test;

import ntust.idsl.book.client.stub.BookServiceImplStub;
import ntust.idsl.book.client.stub.BookServiceImplStub.PurchaseHistory;

/**
 * @author Carl Adler(C.A.)
 * */
public class PurchaseHistoryHandler {
	
	public void listPurchaseHistory() throws Exception{
		
		BookServiceImplStub stubForHistory = new BookServiceImplStub();
		BookServiceImplStub.ListPurchaseHistoryForMember listHistoryForMember 
		= new BookServiceImplStub.ListPurchaseHistoryForMember();
		listHistoryForMember.setMemberID(1);
		PurchaseHistory[] purchaseHistories = 
				stubForHistory.listPurchaseHistoryForMember(listHistoryForMember).get_return();
		
		for(int i = 0; i < purchaseHistories.length; i++) {
			PurchaseHistory history = purchaseHistories[i];
			System.out.println(history.getOrderID());
			System.out.println(history.getOrderDate());
			System.out.println(history.getTitle());
			System.out.println(history.getAuthor());
			System.out.println(history.getIsbn());
			System.out.println(history.getPublisher());
			System.out.println(history.getPrice());
			System.out.println(history.getAmount());
			System.out.println(history.getDeliveryName());
			System.out.println(history.getTotal());
			System.out.println();
		}
	}
}
