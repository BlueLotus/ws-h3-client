package ntust.idsl.book.client.test;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableColumnModel;

import ntust.idsl.book.client.model.BookModel;
import ntust.idsl.book.client.module.BookListModule;
import ntust.idsl.book.client.stub.BookServiceImplStub.BookInfo;

public class TableTest {
    private JFrame frame = null;
    private JTable table = null;
    private BookModel model = null;
    private JScrollPane s_pan = null;
    private JPanel pane = null;

    public TableTest() throws Exception {
        frame = new JFrame("JTableTest");
        pane = new JPanel();
        
        model = new BookModel();
        table = new JTable(model);
        table.setBackground(Color.white);
        
        
        TableColumnModel tcm = table.getColumnModel();
        tcm.getColumn(0).setPreferredWidth(50);
        tcm.getColumn(1).setPreferredWidth(50);
        tcm.getColumn(2).setPreferredWidth(50);
        tcm.getColumn(3).setPreferredWidth(15);
        tcm.getColumn(4).setPreferredWidth(8);
        tcm.getColumn(5).setPreferredWidth(8);

        s_pan = new JScrollPane(table);
        addData();

        frame.getContentPane().add(s_pan, BorderLayout.CENTER);
        frame.getContentPane().add(pane, BorderLayout.NORTH);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 200);
        frame.setVisible(true);

    }

    private void addData() throws Exception {
    	BookInfo[] bookList= BookListModule.obatinBookList();
    	for (BookInfo book : bookList) {
			model.addRow(book);
		}
        table.updateUI();
    }

    public static void main(String args[]) throws Exception {
        new TableTest();
    }

}
