package ntust.idsl.book.client.test;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Ignore;
import org.junit.Test;

public class DateTest {

	@Test
	@Ignore
	public void test() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Long currentTime = new Date().getTime();
		Date date = new Date(currentTime);
		
		System.out.println(date.toString());
		System.out.println(sdf.format(date));
		Date newDate = sdf.parse(sdf.format(date));
		System.out.println(newDate);
	}
	
	@Test
	public void genOrderId() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMddhhmm");
		Long currentTime = new Date().getTime();
		Date date = new Date(currentTime);
		int orderId = Integer.valueOf(sdf.format(date));
		System.out.println(orderId);
	}

}
