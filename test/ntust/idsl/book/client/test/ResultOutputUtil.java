package ntust.idsl.book.client.test;

import ntust.idsl.book.client.stub.BookServiceImplStub.BookInfo;
import ntust.idsl.book.client.stub.BookServiceImplStub.EncryptBookOrder;
import ntust.idsl.book.client.stub.BookServiceImplStub.PurchaseHistory;

public class ResultOutputUtil {
	
	public static void outputResult(BookInfo bookInfo){
		System.out.println(bookInfo.getBookId());
		System.out.println(bookInfo.getTitle());
		System.out.println(bookInfo.getIsbn());
		System.out.println(bookInfo.getAuthor());
		System.out.println(bookInfo.getPrice());
		System.out.println(bookInfo.getPublisher());
	}
	
	public static void outputBookOrder(EncryptBookOrder bookOrder) {
		System.out.println("OrderId: "+ bookOrder.getOrderID());
		System.out.println("MemberId: "+ bookOrder.getMemberID());
		System.out.println("OrderDate: "+ bookOrder.getOrderDate());
		System.out.println("DeliveryId: "+ bookOrder.getDeliveryID());
		System.out.println("Amount: "+ bookOrder.getAmount());
		System.out.println("BookId: "+ bookOrder.getBookID());
	}
	
	public static void outputPurchaseHistory(PurchaseHistory history) {
		System.out.println("OrderId: " + history.getOrderID());
		System.out.println("OrderDate: " + history.getOrderDate());
		System.out.println("Title: " + history.getTitle());
		System.out.println("Author: " + history.getAuthor());
		System.out.println("ISBN: " + history.getIsbn());
		System.out.println("Publisher: " + history.getPublisher());
		System.out.println("Price: " + history.getPrice());
		System.out.println("Quantity: " + history.getAmount());
		System.out.println("Delivery Method: " + history.getDeliveryName());
		System.out.println("Total: " + history.getTotal());
	}
	
}
