package ntust.idsl.book.client.test;


/**
 * @author Carl Adler(C.A.)
 * */
public class TestMain {
	
	public static void main(String[] args) {
		/*LoginHandler loginHandler = new LoginHandler();
		try {
			int loginResult = loginHandler.login("yotsuba1", "12345");
			System.out.println("Login result: Member ID is  " + loginResult);
		} catch (Exception e) {
			System.out.println("Login mechanism is malfunction, please trace the information in log file...");
			e.printStackTrace();
		}
		
		System.out.println("Starting to handle the order...\n\n");
		OrderHandler orderHandler = new OrderHandler();
		try {
			String orderResult = orderHandler.orderBooks();
			System.out.println("Order result: " + orderResult);
		} catch (Exception e) {
			System.out.println("Order mechanism is malfunction, please trace the information in log file...");
			e.printStackTrace();
		}*/
		
		System.out.println("Starting to list purchase history...");
		try {
			PurchaseHistoryHandler purchaseHistoryHandler = new PurchaseHistoryHandler();
			purchaseHistoryHandler.listPurchaseHistory();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
