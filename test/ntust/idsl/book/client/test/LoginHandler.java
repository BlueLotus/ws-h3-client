package ntust.idsl.book.client.test;

import ntust.idsl.book.client.stub.BookServiceImplStub;
import ntust.idsl.book.client.stub.BookServiceImplStub.VerifyMember;

/**
 * @author Carl Adler(C.A.)
 * */
public class LoginHandler {
	
	public int login(String account, String password) throws Exception{
		BookServiceImplStub stubForLogin = new BookServiceImplStub();
		BookServiceImplStub.VerifyMember verify =  new BookServiceImplStub.VerifyMember();
		verify.setAccount(account);
		verify.setPassword(password);
		return stubForLogin.verifyMember(verify).get_return();
	}
}
